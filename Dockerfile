FROM ubuntu:18.04

RUN apt-get update; apt-get --yes install python3 \
    python3-nacl python3-pip libffi-dev python3-pip git

RUN pip3 install ansible

RUN mkdir /root/sopo
WORKDIR /root/sopo

ADD ./playbooks playbooks/
ADD ./app-role roles
ADD ./environments environments/
ADD ./ansible.cfg ansible.cfg

RUN ansible-galaxy install -r roles/requirements.yml

RUN ansible-playbook --connection=local --inventory 127.0.0.1, playbooks/django.yml -D